</main>

<?php show_admin_bar(true);?>
<?php wp_footer(); ?>
<footer class="main-footer">
	<p>&copy; 
		<?php echo date('Y'); ?> 
		<a href="<?php echo site_url(); ?>" title="<?php echo bloginfo('title'); ?> Website">
			<?php echo bloginfo('title'); ?> 
		</a>
	</p>
</footer>
</body>
</html>