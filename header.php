<!DOCTYPE html>
<html>
<head>
    <meta charset="utf-8">
    <meta http-equiv="X-UA-Compatible" content="IE=edge">
    <meta name="viewport" content="width=device-width, initial-scale=1">
	<title></title>
	<?php do_action('wp_head');?>
	<link href='https://fonts.googleapis.com/css?family=Roboto+Slab:400,700,300,100' rel='stylesheet' type='text/css'>
	<link href="https://fonts.googleapis.com/css?family=Roboto+Mono:100,100i,300,300i,400,400i,500,500i,700,700i|Roboto:100,100i,300,300i,400,400i,500,500i,700,700i" rel="stylesheet">
</head>
<body>
<header class="main">
	<h1><a href="<?php echo site_url() ?>" title="Visit <?php bloginfo('title');?> Front Page"><?php bloginfo('title');?></a></h1>
</header>
<nav class="main-nav">
	<?php wp_nav_menu( 'primary' ); ?>
	<a href="#menu-main" class="btn visible-xs" title="Go to <?php bloginfo('title');?> Main Menu"><span class="glyphicon glyphicon-info-sign"></span></a>
</nav>

<main class="wrapper">



