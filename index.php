<?php
/**
 * Main Template
 * 
 * This is the main site template which is used when 
 * there are no more specific templates to use.
 */

?>

<?php get_header(); ?>

<?php while (have_posts() ) : the_post() ?>

    <article>
        <header>
            <h2><?php the_title(); ?></h2>
        </header>
        <section class="main">
            <?php the_content(); ?>
        </section>
        <footer>
            <a href="<?php the_permalink();?>" title="Read <?php the_title(); ?>" class="btn">
                Read More
            </a>
        </footer>
    </article>

<?php endwhile; ?>

<?php get_footer(); ?>
