<?php 

class ThemeHelper {
	/**
	 * Stores the path references for theme assets.
	 * @var array
	 */
	protected static $dir = array(
		'css'   => 'css',
		'fonts' => 'fonts',
		'img'   => 'img',
		'js'    => 'css'
	);
	/**
	 * Gets the requested asset directory and returns its 
	 * absolute location.
	 * 
	 * @param  string $dir The name of the asset directory.
	 * @return string      Its absolute path.
	 */
	public static function getDir($dir) {
		if (array_key_exists($dir, self::$dir))
			return get_stylesheet_directory_uri() . '/' . self::$dir[$dir] . '/';
	}

	public function init() {
		$this->add_actions();
		$this->add_filters();
		$this->registerStyles();
	}

	/**
	 * Add feature support declarations here.
	 */
	public function add_theme_support() {
		add_theme_support( 'menus' );
		add_theme_support( 'post-thumbnails' );
	}

	public function add_actions() {
		/* Register theme menus */
		add_action( 'init', ['ThemeHelper','registerMenus'] );
		/* Add theme support */
		add_action( 'after_setup_theme', ['ThemeHelper','add_theme_support'] );
		/* Load stylesheets */
		add_action('wp_head', ['ThemeHelper','registerStyles']);
		add_action('wp_enqueue_scripts', ['ThemeHelper','registerScripts']);
	}
	public function add_filters() {
		add_filter( 'loop_shop_per_page', create_function( '$cols', 'return 6;' ), 20 );
	}

	public static function registerMenus() {
  		register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
	}

	/**
	 * Registers the stylesheets used by this theme.
	 * @return void
	 */
	public function registerStyles() {
	
		wp_register_style('bootstrap-css', self::getDir('css') . 'bootstrap.min.css' );
		wp_enqueue_style('bootstrap-css', self::getDir('css') . 'bootstrap.min.css' );
		
		wp_register_style('theme-css', self::getDir('css') . 'app.css' );
		wp_enqueue_style('theme-css', self::getDir('css') . 'app.css' );
	
	}

	/**
	 * Registers the javascripts used by this theme.
	 * @return void
	 */
	public function registerScripts() {
		// wp_register_script( 'custom-script', plugins_url( '/js/custom-script.js', __FILE__ ) );
	    wp_register_script( 'bootstrap-js', self::getDir('js') . '/bootstrap.min.js' );
	    wp_enqueue_script( 'custom-script' );
	}

	public function setTitle($title) {
		echo "Setting title";
		$this->pageTitle = title;
	} 

	public function headTitle() {
		echo $this->pageTitle . ' - ' . bloginfo('title');
	}
}
/*
 * CUSTOM GLOBAL VARIABLES
 */
function trhq_theme_global_vars() {
    global $theme;
    $theme = new ThemeHelper;
    $theme->init();
}
add_action( 'parse_query', 'trhq_theme_global_vars' );
function theme() {
	global $theme;
	return $theme;
}

function registerScripts() {
	// wp_register_script( 'custom-script', plugins_url( '/js/custom-script.js', __FILE__ ) );
    wp_register_script( 'bootstrap-js', get_template_directory_uri() . '/js/bootstrap.min.js' );
    wp_enqueue_script( 'bootstrap-js' );
}

add_action('wp_enqueue_scripts', 'registerScripts');
// add_action('wp_head','registerStyles');

function registerMenus() {
	register_nav_menu( 'primary', __( 'Primary Menu', 'theme-slug' ) );
}
add_action( 'init', 'registerMenus');